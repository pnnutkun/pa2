package ku.util;

import java.util.EmptyStackException;
/**
 * The Java API has a Stack interface, but some of the method names are inconsistent with other collections,
 * and it has a search method which doesn't make sense for a Stack. So, I will define my own Stack type.
 * @author Pipatpol Tanavongchinda
 *
 * @param <T> is a type parameter.
 */
public class Stack<T> {
	/** Items on the stack.*/
	private T[] items;

	/**
	 * Create a new stack with the given capacity. Capacity must be positive.
	 * @param capacity is size of stack.
	 */
	public Stack(int capacity)
	{
		if(capacity < 0)
		{
			capacity = 0;	
		}
		items = (T[]) new Object[capacity];
	}
	/**
	 * Show the maximum number that stack can take all items.
	 * @return Return the maximum number of elements that this Stack can hold. Return -1 
	 * if unknown or infinite.
	 */
	public int capacity()
	{
		if(items == null)
		{
			return -1;
		}
		return items.length;
	}
	/**
	 * Check the stack is empty.
	 * @return true if stack is empty.
	 */
	public boolean isEmpty()
	{
		for ( int count = 0 ; count < items.length ; count++ )
		{
			if( items[count] != null )
			{
				return false;
			}
		}
		return true;
	}
	/**
	 * Check the stack is full.
	 * @return true if stack is full.
	 */
	public boolean isFull()
	{
		return size() == items.length;
	}
	/**
	 * Look on the top of stack.
	 * @return return the item on the top of the stack, without removing it. If the stack is 
	 * empty, return null.
	 */
	public T peek()
	{
		if ( !isEmpty() )
		{
			for ( int count = items.length-1 ; count >= 0 ; count-- )
			{
				if( items[count] != null )
				{
					return items[count];
				}
			}
		}
		return null;
	}
	/**
	 * Pop out the top of stack.
	 * @return return the item on the top of the stack, and remove it from the stack.
	 * Throws: EmptyStackException if stack is empty.
	 */
	public T pop ( )
	{
		if( !isEmpty( ) )
		{
			T out = peek();
			for ( int count = items.length-1 ; count >= 0 ; count-- )
			{
				if( items[count] != null )
				{
					items[count] = null;
					break;
				}
			}
			return out;
		}
		throw new EmptyStackException( );
	}
	/**
	 * push a new item onto the top of the stack. If the stack is already full, this
	 * method does nothing... its the programmer's responsibility to check isFull()
	 * before trying to push something onto stack.
	 * @param obj must not be null.
	 * Throws: InvalidArgumentException if parameter is null.
	 */
	public void push( T obj )
	{
		if( obj == null )
		{
			throw new IllegalArgumentException( );
		}
		if( !isFull() )
		{
			for ( int count = 0 ; count < items.length ; count++ )
			{
				if( items[count] == null )
				{
					items[count] = obj;
					break;
				}
			}
		}
	}
	/**
	 * Show size of this stack at that time.
	 * @return return the number of items in the stack. Returns 0 if the stack is empty.
	 */
	public int size ( )
	{
		if ( isEmpty() )
		{
			return 0;
		}
		else {
			int countItem = 0;
			for ( int count = 0 ; count < items.length ; count++ )
			{
				if( items[count] != null )
				{
					countItem++;
				}
			}
			return countItem;
		}
	}
}
